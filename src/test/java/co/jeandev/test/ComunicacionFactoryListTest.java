/**
 * 
 */
package co.jeandev.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import co.jeandev.factory.ComunicacionFactoryList;
import co.jeandev.model.Comunicacion;
import co.jeandev.tipos.comunicacion.TiposComunicacion;

/**
 * @author jean
 *
 */
public class ComunicacionFactoryListTest {
	private static ComunicacionFactoryList factoryList;
	
	@BeforeClass
	public static void init() {
		factoryList = new ComunicacionFactoryList();
	}
	
	@Test
	public void testFactorySimple() {
		int numero = 30;
		List<Comunicacion> list = factoryList.getComunicacion(TiposComunicacion.LLAMADA, numero);
		assertEquals(numero, list.size());
	}
}
