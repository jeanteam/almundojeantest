/**
 * 
 */
package co.jeandev.test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.BeforeClass;
import org.junit.Test;

import co.jeandev.factory.EmpleadoFactory;
import co.jeandev.model.Director;
import co.jeandev.model.Empleado;
import co.jeandev.tipos.comunicacion.TipoEmpleado;

/**
 * @author jean
 *
 */
public class EmpleadoFactoryTest {

	private static EmpleadoFactory empleadoFactory;
	
	@BeforeClass
	public static void init() {
		empleadoFactory = new EmpleadoFactory();
	}
	
	@Test
	public void testSimple() {
		Empleado empleado = empleadoFactory.getEmpleado(TipoEmpleado.DIRECTOR);
		assertThat(empleado, instanceOf(Director.class));
	}
}
