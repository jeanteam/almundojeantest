package co.jeandev.test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import co.jeandev.factory.ComunicacionFactory;
import co.jeandev.model.Comunicacion;
import co.jeandev.model.Llamada;
import co.jeandev.tipos.comunicacion.TiposComunicacion;

public class ComunicacionFactoryTest {

	private static ComunicacionFactory comunicacionFactory;

	@BeforeClass
	public static void init() {
		comunicacionFactory = new ComunicacionFactory();
	}
	
	@Before
	public void beforeTest() {
		System.out.println("Inicio del test");
	}

	@After
	public void afterTest() {
		System.out.println("Fin del test");
	}

	/**
	 * Realiza el test para validar que la instancia del factory es de tipo llamada
	 */
	@Test
	public void testFactoryLlamada() {
		Comunicacion llamada = comunicacionFactory.getComunicacion(TiposComunicacion.LLAMADA);
		assertThat(llamada, instanceOf(Llamada.class));
	}
}
