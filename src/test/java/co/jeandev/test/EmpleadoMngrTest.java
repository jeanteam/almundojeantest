/**
 * 
 */
package co.jeandev.test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import co.jeandev.mngr.EmpleadoMngr;
import co.jeandev.model.Director;
import co.jeandev.model.Empleado;
import co.jeandev.model.Operador;
import co.jeandev.model.Supervisor;

/**
 * @author jean
 *
 */
public class EmpleadoMngrTest {

	@Test
	public void testOperadorDisponible() {
		EmpleadoMngr empleadoMngr  = new EmpleadoMngr(1);
		Empleado empleado = empleadoMngr.getEmpleadoDisponible();
		assertThat(empleado, instanceOf(Operador.class));
	}

	@Test
	public void testSupervisorDisponible() {
		EmpleadoMngr empleadoMngr  = new EmpleadoMngr(1);
		empleadoMngr.getEmpleadoDisponible();//Operador
		Empleado empleado = empleadoMngr.getEmpleadoDisponible();
		assertThat(empleado, instanceOf(Supervisor.class));
	}

	@Test
	public void testDirectorDisponible() {
		EmpleadoMngr empleadoMngr  = new EmpleadoMngr(1);
		empleadoMngr.getEmpleadoDisponible();//Operador
		empleadoMngr.getEmpleadoDisponible();//Supervisor
		Empleado empleado = empleadoMngr.getEmpleadoDisponible();
		assertThat(empleado, instanceOf(Director.class));
	}

}
