/**
 * 
 */
package co.jeandev.test;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.BeforeClass;
import org.junit.Test;

import co.jeandev.factory.ComunicacionFactoryList;
import co.jeandev.mngr.EmpleadoMngr;
import co.jeandev.model.Comunicacion;
import co.jeandev.service.Dispatcher;
import co.jeandev.tipos.comunicacion.TiposComunicacion;

/**
 * @author jean
 *
 */
public class DispatcherTest {
	private static final int NUMERO_LLAMADAS_A_CREAR = 10;
	private static List<Comunicacion> llamadas;
	private static EmpleadoMngr empleadoMngr;

	@BeforeClass
	public static void init() {
		llamadas = new ComunicacionFactoryList().getComunicacion(TiposComunicacion.LLAMADA, NUMERO_LLAMADAS_A_CREAR);
		empleadoMngr = new EmpleadoMngr(1);
	}

	@Test
	public void test() throws InterruptedException, ExecutionException, TimeoutException {
		Dispatcher.distpachCall(llamadas, empleadoMngr);
	}

}
