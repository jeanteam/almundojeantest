/**
 * 
 */
package co.jeandev.tipos.comunicacion;

/**
 * @author jean
 *
 */
public enum TipoEmpleado {
	OPERADOR, SUPERVISOR, DIRECTOR
}
