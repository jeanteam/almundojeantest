/**
 * 
 */
package co.jeandev.factory;

import co.jeandev.model.Comunicacion;
import co.jeandev.model.Llamada;
import co.jeandev.tipos.comunicacion.TiposComunicacion;

/**
 * @author jean
 *
 */
public class ComunicacionFactory {
	private static final String MSG_EXEP_TIPO_NO_DEFINIDO = "El tipo de comunicacion no esta definido";
	private TiposComunicacion tipo;

	public ComunicacionFactory() {
	}

	public ComunicacionFactory(TiposComunicacion tipo) {
		this.tipo = tipo;
	}

	/**
	 * Sobrecarga con en numero del telefono de quien llama
	 */
	public Comunicacion getComunicacion(TiposComunicacion tipo) {
		switch (tipo) {
		case LLAMADA:
			return new Llamada();
		default:
			return null;
		}
	}

	/**
	 * Sobrecarga del metodo para que sirva como supplier del presente factory
	 */
	public Comunicacion getComunicacion() {
		if (tipo == null) {
			throw new IllegalArgumentException(MSG_EXEP_TIPO_NO_DEFINIDO);
		}
		return getComunicacion(tipo);
	}
}
