/**
 * 
 */
package co.jeandev.factory;

import co.jeandev.model.Director;
import co.jeandev.model.Empleado;
import co.jeandev.model.Operador;
import co.jeandev.model.Supervisor;
import co.jeandev.tipos.comunicacion.TipoEmpleado;

/**
 * @author jean
 *
 */
public class EmpleadoFactory {

	private static final String MSG_TIP_EMP_NO_VALIDO = "El tipo de empleado no está disponible.";

	public Empleado getEmpleado(TipoEmpleado tipo) {
		switch (tipo) {
		case OPERADOR:
			return new Operador();

		case SUPERVISOR:
			return new Supervisor();

		case DIRECTOR:
			return new Director();

		default:
			throw new IllegalArgumentException(MSG_TIP_EMP_NO_VALIDO);
		}
	}
}
