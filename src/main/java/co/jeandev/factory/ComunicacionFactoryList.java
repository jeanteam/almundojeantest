/**
 * 
 */
package co.jeandev.factory;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import co.jeandev.model.Comunicacion;
import co.jeandev.tipos.comunicacion.TiposComunicacion;

/**
 * Factory en batch
 * 
 * @author jean
 *
 */
public class ComunicacionFactoryList {

	/**
	 * Método Factory de tipo colecction List 
	 */
	public List<Comunicacion> getComunicacion(TiposComunicacion tipo, int cantidad) {
		ComunicacionFactory comunicacion = new ComunicacionFactory(tipo);
		return Stream.generate(comunicacion::getComunicacion).limit(cantidad).collect(Collectors.toList());
	}
}
