/**
 * 
 */
package co.jeandev.service;

import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import co.jeandev.mngr.EmpleadoMngr;
import co.jeandev.model.Comunicacion;
import co.jeandev.model.Empleado;
import co.jeandev.model.Llamada;

/**
 * Se encarga de manejar las llamadas
 * 
 * @author jean
 *
 */
public class Dispatcher implements IDispacher {

	private static final String MSG_TIPO_COMU_NO_IMPLEM = "Solo es posible atender llamadas reales.";
	private static final int TIEMPO_ESPERA = 10;
	private static final int NUMERO_LLAMADAS_CONCURRENTES = 10;
	private final EmpleadoMngr empleadoMgr;
	private static ExecutorService service;
	private ConcurrentLinkedQueue<Comunicacion> llamadas;
	static {
		service = Executors.newFixedThreadPool(NUMERO_LLAMADAS_CONCURRENTES);
	}

	public Dispatcher(List<Comunicacion> llamadas, EmpleadoMngr empleadoMgr) {
		this.llamadas = new ConcurrentLinkedQueue<>(llamadas);
		this.empleadoMgr = empleadoMgr;
	}

	/**
	 * Despachar llamada
	 * 
	 */
	@Override
	public void dispatchCall() {
		Empleado empleado = null;
		empleado = empleadoMgr.getEmpleadoDisponible();
		if (empleado != null) {
			Comunicacion llamada = llamadas.poll();
			synchronized (llamada) {
				empleado.callResponse(llamada);
				empleado.setBusy(false);
			}
			try {
				TimeUnit.SECONDS.sleep(llamada.getDuracion());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Punto de inicio para resolver las llamadas
	 * 
	 */
	public static void distpachCall(List<Comunicacion> llamadas, EmpleadoMngr empleadoMngr)
			throws InterruptedException, ExecutionException, TimeoutException {
		Dispatcher dispatcher = new Dispatcher(llamadas, empleadoMngr);

		for (Comunicacion comunicacion : llamadas) {
			service.submit(() -> dispatcher.dispatchCall());
		}
		service.shutdown();
		service.awaitTermination(Long.MAX_VALUE, TimeUnit.MINUTES);
	}

}
