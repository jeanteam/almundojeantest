/**
 * 
 */
package co.jeandev.mngr;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import co.jeandev.factory.EmpleadoFactory;
import co.jeandev.model.Empleado;
import co.jeandev.tipos.comunicacion.TipoEmpleado;

/**
 * Manager de Empleado. Los crea y regresa los disponibles.
 * 
 * @author jean
 *
 */
public class EmpleadoMngr {
	private List<Empleado> operador;
	private List<Empleado> supervisor;
	private List<Empleado> director;

	public EmpleadoMngr(int cantidad) {
		operador = new CopyOnWriteArrayList <>();
		supervisor = new CopyOnWriteArrayList <>();
		director = new CopyOnWriteArrayList <>();
		EmpleadoFactory empleadoFactory = new EmpleadoFactory();
		for (int i = 0; i < cantidad; i++) {
			operador.add(empleadoFactory.getEmpleado(TipoEmpleado.OPERADOR));
			supervisor.add(empleadoFactory.getEmpleado(TipoEmpleado.SUPERVISOR));
			director.add(empleadoFactory.getEmpleado(TipoEmpleado.DIRECTOR));
		}
	}

	/**
	 * Retorna el empleado disponible empezando por Operador, luego Supervisor y por
	 * último Director.
	 */
	public synchronized Empleado getEmpleadoDisponible() {
		Empleado empleadoDisponible = null;
		if ((empleadoDisponible = getEmpleadoDisponible(TipoEmpleado.OPERADOR)) != null) {
			return empleadoDisponible;
		} else if ((empleadoDisponible = getEmpleadoDisponible(TipoEmpleado.SUPERVISOR)) != null) {
			return empleadoDisponible;
		} else {
			return getEmpleadoDisponible(TipoEmpleado.DIRECTOR);
		}
	}

	/**
	 * Determina el tipo de empleado. Sobrecarga y ayuda del metodo sobrecargado
	 */
	private Empleado getEmpleadoDisponible(TipoEmpleado tipoEmpleado) {
		List<Empleado> empleados = getListEmpleado(tipoEmpleado);
		for (Empleado empleado : empleados) {
			if (!empleado.isBusy()) {
				empleado.setBusy(true);
				return empleado;
			}
		}
		return null;
	}

	/**
	 * Determina y retorna la lista del tipo especifico de empleado
	 */
	private List<Empleado> getListEmpleado(TipoEmpleado tipoEmpleado) {
		switch (tipoEmpleado) {
		case OPERADOR:
			return operador;
		case SUPERVISOR:
			return supervisor;
		case DIRECTOR:
			return director;
		default:
			return null;
		}
	}
}
