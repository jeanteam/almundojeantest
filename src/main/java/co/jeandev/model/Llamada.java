package co.jeandev.model;

/**
 * @author jean
 */
public class Llamada implements Comunicacion {
	private static int consecutivoGeneral;

	/**
	 * Numero auto-consecutivo de la llamada
	 */
	private int consecutivo;

	/**
	 * Random de la duracion en segundos de la llamada
	 * */
	private int duracion;

	/**
	 * Constructor con un argumento
	 */
	public Llamada() {
		consecutivo = ++consecutivoGeneral;
		duracion = randomTime();
	}

	@Override
	public int getConsecutivo() {
		return consecutivo;
	}

	@Override
	public int getDuracion() {
		return duracion;
	}

}
