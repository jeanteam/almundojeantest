/**
 * 
 */
package co.jeandev.model;

import co.jeandev.tipos.comunicacion.TipoEmpleado;

/**
 * @author Jean
 *
 */
public class Operador implements Empleado {

	private boolean isBusy;
	
	private TipoEmpleado tipoEmplado;
	
	public Operador() {
		tipoEmplado = TipoEmpleado.OPERADOR;
	}

	@Override
	public void callResponse(Comunicacion llamada) {
		System.out.println("Operador respondiendo la llamada #" + llamada.getConsecutivo() + ". Tiempo de la llamada:"
				+ llamada.getDuracion()+ " segundo(s)");
	}

	@Override
	public boolean isBusy() {
		return isBusy;
	}

	@Override
	public void setBusy(boolean isBusy) {
		this.isBusy = isBusy;
	}

	/**
	 * @return the tipoEmplado
	 */
	public TipoEmpleado getTipoEmplado() {
		return tipoEmplado;
	}

	/**
	 * @param tipoEmplado the tipoEmplado to set
	 */
	public void setTipoEmplado(TipoEmpleado tipoEmplado) {
		this.tipoEmplado = tipoEmplado;
	}
}
