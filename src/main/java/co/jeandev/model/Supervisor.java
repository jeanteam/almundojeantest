package co.jeandev.model;

import co.jeandev.tipos.comunicacion.TipoEmpleado;

public class Supervisor implements Empleado {

	private boolean isBusy;

	private TipoEmpleado tipoEmplado;

	public Supervisor() {
		tipoEmplado = TipoEmpleado.SUPERVISOR;
	}

	@Override
	public void callResponse(Comunicacion llamada) {
		System.out.println("Supervisor respondiendo la llamada #" + llamada.getConsecutivo() + ". Tiempo de la llamada:"
				+ llamada.getDuracion() + " segundo(s)");
	}

	@Override
	public boolean isBusy() {
		return isBusy;
	}

	@Override
	public void setBusy(boolean isBusy) {
		this.isBusy = isBusy;
	}

	/**
	 * @return the tipoEmplado
	 */
	public TipoEmpleado getTipoEmplado() {
		return tipoEmplado;
	}

	/**
	 * @param tipoEmplado
	 *            the tipoEmplado to set
	 */
	public void setTipoEmplado(TipoEmpleado tipoEmplado) {
		this.tipoEmplado = tipoEmplado;
	}

}
