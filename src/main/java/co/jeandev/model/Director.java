package co.jeandev.model;

import co.jeandev.tipos.comunicacion.TipoEmpleado;

public class Director implements Empleado {
	private boolean isBusy;

	private TipoEmpleado tipoEmplado;

	public Director() {
		tipoEmplado = TipoEmpleado.DIRECTOR;
	}

	@Override
	public void callResponse(Comunicacion llamada) {
		System.out.println("Director Respondiendo la llamada #" + llamada.getConsecutivo() + ". Tiempo de la llamada:"
				+ llamada.getDuracion() + " segundo(s)");
	}

	@Override
	public boolean isBusy() {
		return isBusy;
	}

	@Override
	public void setBusy(boolean isBusy) {
		this.isBusy = isBusy;
	}

	/**
	 * @return the tipoEmplado
	 */
	public TipoEmpleado getTipoEmplado() {
		return tipoEmplado;
	}

	/**
	 * @param tipoEmplado
	 *            the tipoEmplado to set
	 */
	public void setTipoEmplado(TipoEmpleado tipoEmplado) {
		this.tipoEmplado = tipoEmplado;
	}

}
