package co.jeandev.model;

import java.util.Random;

/**
 * @author jean
 */
public interface Comunicacion {
	int DURACION_MAX = 10;
	int DURACION_MIN = 5;

	int getConsecutivo();

	int getDuracion();

	/**
	 * Tiempo de la llamada en un Random
	 */
	default int randomTime() {
		return new Random().nextInt((DURACION_MAX - DURACION_MIN) + 1) + DURACION_MIN;
	}
}