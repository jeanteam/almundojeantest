package co.jeandev.model;

import co.jeandev.tipos.comunicacion.TipoEmpleado;

public interface Empleado {

	/**
	 * Método encargado de atender la llamada
	 */
	void callResponse(Comunicacion comunicacion);

	/**
	 * Deteramina si esta ocupado el empleado
	 */
	boolean isBusy();

	/**
	 * Setea el estado del empleado
	 */
	void setBusy(boolean isBusy);

	TipoEmpleado getTipoEmplado();

	void setTipoEmplado(TipoEmpleado tipoEmplado);

}